﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Markup;
using System.Windows.Media;
using Brush = System.Windows.Media.Brush;

namespace DesktopUniversalFrame.Common.Markup
{
    public class ThemeExtension : MarkupExtension
    {
        public Brush ThemeColor { get; set; }
        public string ThemeName { get; set; }

        public ThemeExtension()
        {

        }

        public ThemeExtension(string _themeName) : base()
        {
            ThemeName = _themeName;
        }

        public ThemeExtension(Brush _themeColor)
        {
            ThemeColor = _themeColor;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (ThemeColor != null)
                return ThemeColor;
            else
                return null;
        }
    }
}
